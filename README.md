# Zeolite
Just a multifunctional bot that simple in terms of economy.

Zeolite v3.x is **NO LONGER supported**, we do everything in a [new branch](https://github.com/MicroPizdec/Zeolite/tree/v4).

Made with ❤️ & written on JavaScript with Eris library.
